{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE TemplateHaskell    #-}
{-# LANGUAGE TypeFamilies       #-}
module Main where

import           Data.Acid                            (AcidState, Query, Update,
                                                       makeAcidic,
                                                       openLocalState, query,
                                                       update)
import           Data.Acid.Local                      (createCheckpointAndClose)
import           Data.Map                             (Map, empty, insert,
                                                       lookup, toList)
import           Data.SafeCopy                        (base, deriveSafeCopy)
import           Data.Text.Lazy                       (Text, fromStrict)
import           Data.Text.Lazy.Encoding              (encodeUtf8)
import           Data.Typeable                        (Typeable)

import           Control.Exception                    (bracket)
import           Data.Attoparsec.Number               (Number (..))
import           Data.ByteString.Base64               (decode)
import           Data.ByteString.Lazy                 (toStrict)

import           Control.Monad.Reader                 (ask)
import           Control.Monad.State                  (forM_, liftIO, mzero)
import qualified Control.Monad.State                  as ST
import           Data.Aeson                           (FromJSON (..),
                                                       ToJSON (..), Value (..),
                                                       object, (.:), (.=))
import           Data.ByteString.Char8                (ByteString, writeFile)

import           Network.HTTP.Types                   (status400, status403)
import           Network.Wai.Middleware.RequestLogger (logStdoutDev)
import           Network.Wai.Middleware.Static        (noDots, staticPolicy)

import           Text.Blaze                           (ToMarkup (..), toValue)
import           Text.Blaze.Html.Renderer.Text        (renderHtml)
import           Text.Blaze.Html5                     (a, body, br, head,
                                                       header, html, img, li, p,
                                                       title, ul, (!))
import           Text.Blaze.Html5.Attributes          (alt, href, src)

import           Web.Scotty                           hiding (body, html)
import qualified Web.Scotty                           as W

import           Prelude                              hiding (head, lookup,
                                                       writeFile)

newtype Description = Description Text deriving (Show, Eq, Typeable)
newtype Color = Color Text deriving (Show, Eq, Typeable)
newtype Size = Size Int deriving (Show, Eq, Typeable)
data Photo = H Text
           | F FilePath deriving (Show, Eq, Typeable)

unPhoto :: Photo -> ByteString
unPhoto (H x) = toStrict $ encodeUtf8 x
unPhoto _ = error "bad request unPhoto"

instance ToMarkup Description where
    toMarkup (Description x) = toMarkup x
    preEscapedToMarkup (Description x) = preEscapedToMarkup x

instance ToMarkup Color where
    toMarkup (Color x) = toMarkup x
    preEscapedToMarkup (Color x) = preEscapedToMarkup x

instance ToMarkup Size where
    toMarkup (Size x) = toMarkup x
    preEscapedToMarkup (Size x) = preEscapedToMarkup x

data Shoe = Shoe
  { description :: Description
  , color       :: Color
  , size        :: Size
  , photo       :: Photo
  } deriving (Show, Eq, Typeable)

instance FromJSON Size where
    parseJSON (String s) = case readEither (fromStrict s) of
                                Left _ -> mzero
                                Right s' -> return $ Size s'
    parseJSON (Number s) = case s of
                                I i -> return $ Size $ fromInteger i
                                D _ -> mzero
    parseJSON _ = mzero

instance FromJSON Shoe where
    parseJSON (Object v) = do
        d <- v .: "description"
        c <- v .: "color"
        s <- v .: "size"
        p' <- v .: "photo"
        return $ Shoe (Description d) (Color c) s (H p')
    parseJSON _ = mzero

instance ToJSON Shoe where
    toJSON (Shoe (Description d) (Color c) (Size s) (F p')) = object [ "description" .= d
                                                                     , "color"       .= c
                                                                     , "size"        .= s
                                                                     , "photo"       .= p'
                                                                     ]
    toJSON _ = error "bad shoe"

data Shoes = Shoes Int (Map Int Shoe) deriving (Eq, Show, Typeable)

$(deriveSafeCopy 0 'base ''Description)
$(deriveSafeCopy 0 'base ''Color)
$(deriveSafeCopy 0 'base ''Size)
$(deriveSafeCopy 0 'base ''Photo)
$(deriveSafeCopy 0 'base ''Shoe)
$(deriveSafeCopy 0 'base ''Shoes)

addShoe :: Shoe -> Update Shoes Int
addShoe new = do
    Shoes i m <- ST.get
    ST.put $ Shoes (i + 1) (insert (i + 1) (new { photo = F $ "pics/" ++ show (i + 1) ++ ".jpg" }) m)
    return (i + 1)

getShoe :: Int -> Query Shoes (Maybe Shoe)
getShoe i = do
    Shoes _ m <- ask
    return $ lookup i m

getShoes :: Query Shoes [(Int, Shoe)]
getShoes = do
    Shoes _ m <- ask
    return $ toList m

$(makeAcidic ''Shoes ['addShoe, 'getShoe, 'getShoes])

initialShoes :: Shoes
initialShoes = Shoes 0 empty

main :: IO ()
main = bracket (openLocalState initialShoes)
               createCheckpointAndClose
               (scotty 3000 . app )


app :: AcidState Shoes -> ScottyM ()
app acid = do
    middleware logStdoutDev
    middleware $ staticPolicy noDots

    post "/" $ do
        shoe <- jsonData :: ActionM Shoe
        case decode (unPhoto . photo $ shoe) of
             Left _ -> status status400
             Right b -> do
                 i <- liftIO $ update acid (AddShoe shoe)
                 liftIO $ writeFile ("./pics/" ++ show i ++ ".jpg") b

    get "/" $ do
        shoes <- liftIO $ query acid GetShoes
        shoesPage shoes

    get "/:id" $ do
        i <-  param "id"
        shoe <- liftIO $ query acid (GetShoe i)
        case shoe of
             Nothing -> status status403
             Just shoe' -> shoePage shoe'


shoePage :: Shoe -> ActionM ()
shoePage shoes = W.html $ renderHtml
                        $ html $ do
                            head $ title "Shoes"
                            body $ do
                                br
                                header $ toMarkup (description shoes)
                                p $ toMarkup (color shoes)
                                p $ toMarkup (size shoes)
                                img ! src (un $ photo shoes) ! alt "photo here"
                                where
                                un (F x) = toValue x
                                un _ = error "bad photo in shoesPage"

shoesPage :: [(Int, Shoe)] -> ActionM ()
shoesPage shoes = W.html $ renderHtml
                         $ html $ do
                             head $ title "All"
                             body $ ul $ forM_ shoes shoeLine
                             where shoeLine (n, s) = li $ a ! (href . toValue $ '/':show n) $ toMarkup (description s)


